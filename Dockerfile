FROM python:3.10
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ADD main.py .
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD ["python", "./main.py"]